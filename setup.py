from setuptools import setup

setup(name="omega_daq",
      version="0.1",
      description="Parser for Omega DAQ output files.",
      author="Thomas Bischof",
      author_email="tsbischof@lbl.gov",
      url="https://bitbucket.org/tsbischof/omega_daq",

      packages=["omega_daq", "tests"],
      
      test_suite="tests"
      )
